package com.nursery.logistic.model.dto;

import java.io.Serializable;

import com.nursery.logistic.model.entity.BOL;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BOLDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private BOL bol;

    private UserDTO user;

    private String Note;
}
