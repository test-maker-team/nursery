package com.nursery.logistic.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "\"user\"")
@Getter
@Setter
public class UserDTO implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstname;

    private String role;

    private String lastname;

    private String email;

    private String password;

    private String address;
}
