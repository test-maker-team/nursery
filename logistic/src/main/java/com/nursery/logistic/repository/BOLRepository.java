package com.nursery.logistic.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.nursery.logistic.model.entity.BOL;

public interface BOLRepository extends JpaRepository<BOL, Long> {
}
