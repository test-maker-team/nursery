package com.nursery.logistic.controller;

import com.nursery.logistic.model.dto.BOLDTO;
import com.nursery.logistic.model.entity.BOL;
import com.nursery.logistic.model.response.ResponseModel;
import com.nursery.logistic.service.IBOLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bol")
public class BOLController {

    @Autowired
    private IBOLService orderService;

    @GetMapping("/{id}")
    public ResponseModel one(@PathVariable Long id) {
        try {
            BOL odr = orderService.findById(id);
            BOLDTO lddto = new BOLDTO();
            lddto.setBol(odr);
//    		lddto.setUser(usrClient.getUserByID(odr.getUserId()));
//    		lddto.setNote(usrClient.getError());
            return new ResponseModel(true, lddto);
        } catch (Exception e) {
            return new ResponseModel(false, null, e.getMessage());
        }

    }


}